package ru.nsu.fit.popov.springnsu1.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;
import ru.nsu.fit.popov.springnsu1.model.Note;

import java.sql.*;
import java.util.Calendar;
import java.util.Map;
import java.util.stream.Stream;

@RestController
@RequestMapping(path = "/notes")
public class NotesController {

    private static final Logger LOG = LoggerFactory.getLogger(NotesController.class);

    private static final int NOTES = 10000;

    private static final RestException INVALID_DATA_FORMAT =
            new RestException(HttpStatus.BAD_REQUEST, "Invalid data format");

    private static final String SQL_GET = String.format("" +
                    "SELECT %s, %s " +
                    "FROM %s ",
            Note.COLUMN_DATE, Note.COLUMN_CONTENT,
            Note.TABLE
    );

    private static final String SQL_GET_BY_DATE = String.format("" +
                    "SELECT %s, %s " +
                    "FROM %s " +
                    "WHERE %s >= ? AND %s <= ?",
            Note.COLUMN_DATE, Note.COLUMN_CONTENT,
            Note.TABLE,
            Note.COLUMN_DATE, Note.COLUMN_DATE
    );

    private static final String SQL_INSERT = String.format("" +
                    "INSERT INTO %s(%s, %s) " +
                    "VALUES(?, ?)",
            Note.TABLE, Note.COLUMN_DATE, Note.COLUMN_CONTENT
    );

    @Autowired
    private JdbcTemplate jdbc;

    @GetMapping(path = "/all")
    public Note[] getAll() {
        final Stream.Builder<Note> builder = Stream.builder();
        try {
            jdbc.query(SQL_GET, rs -> {
                        builder.accept(new Note(
                                rs.getDate("date"),
                                rs.getString("content"))
                        );
                    }
            );
        } catch (Exception e) {
            LOG.warn("SQL exception:" + e.getMessage());
            throw new RestException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }

        return builder.build().toArray(Note[]::new);
    }

    @GetMapping
    public Note[] getByDate(@RequestParam(name = "from") Date from,
                            @RequestParam(name = "to") Date to) {
        final Stream.Builder<Note> builder = Stream.builder();
        try {
            jdbc.query(SQL_GET_BY_DATE, new Object[]{ from, to }, rs -> {
                        builder.accept(new Note(
                                rs.getDate("date"),
                                rs.getString("content"))
                        );
                    }
            );
        } catch (Exception e) {
            LOG.warn("SQL exception:" + e.getMessage());
            throw new RestException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }

        return builder.build().toArray(Note[]::new);
    }

    @PostMapping
    public ResponseEntity<HttpStatus> post(@RequestBody Map<String, String> json) {
        final String content;
        try {
            content = json.get("content");
        } catch (Exception e) {
            throw INVALID_DATA_FORMAT;
        }

        final Date date = new Date(Calendar.getInstance().getTimeInMillis());
        try {
            jdbc.update(SQL_INSERT, date, content);
        } catch (Exception e) {
            LOG.warn("SQL exception:" + e.getMessage());
            throw new RestException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @DeleteMapping
    public void delete() {
        jdbc.execute(String.format("" +
                        "TRUNCATE %s",
                Note.TABLE
        ));
    }

    @PostMapping(path = "/raw")
    public ResponseEntity<Map> postRaw() {
        double speed;

        final Date now = new Date(System.currentTimeMillis());
        try (final Connection connection = jdbc.getDataSource().getConnection();
             final Statement statement = connection.createStatement()) {
            connection.setAutoCommit(false);

            final long start = System.currentTimeMillis();

            for (int i = 0; i < NOTES; i++) {
                statement.execute(String.format("" +
                                "INSERT INTO %s(%s, %s) " +
                                "VALUES ('%s', 'Note #%d')",
                        Note.TABLE, Note.COLUMN_DATE, Note.COLUMN_CONTENT,
                        now, i
                ));
            }
            connection.commit();

            speed = NOTES / ((System.currentTimeMillis() - start) / 1000.0);
        } catch (SQLException e) {
            LOG.warn("SQL exception:" + e.getMessage());
            throw new RestException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }

        LOG.info("raw: " + speed);

        return new ResponseEntity<>(Map.of("speed", speed), HttpStatus.CREATED);
    }

    @PostMapping(path = "/prepared")
    public ResponseEntity<Map> postPrepared() {
        double speed;

        final Date now = new Date(System.currentTimeMillis());
        try (final Connection connection = jdbc.getDataSource().getConnection();
             final PreparedStatement statement = connection.prepareStatement(SQL_INSERT)) {
            connection.setAutoCommit(false);

            final long start = System.currentTimeMillis();

            for (int i = 0; i < NOTES; i++) {
                statement.setDate(1, now);
                statement.setString(2, "Note #" + i);
                statement.execute();
            }
            connection.commit();

            speed = NOTES / ((System.currentTimeMillis() - start) / 1000.0);
        } catch (SQLException e) {
            throw new RestException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }

        LOG.info("prepared: " + speed);

        return new ResponseEntity<>(Map.of("speed", speed), HttpStatus.CREATED);
    }

    @PostMapping(path = "/batch")
    public ResponseEntity<Map> postBatch() {
        double speed;

        final Date now = new Date(System.currentTimeMillis());
        try (final Connection connection = jdbc.getDataSource().getConnection();
             final PreparedStatement statement = connection.prepareStatement(SQL_INSERT)) {
            connection.setAutoCommit(false);

            final long start = System.currentTimeMillis();

            for (int i = 0; i < NOTES; i++) {
                statement.setDate(1, now);
                statement.setString(2, "Note #" + i);
                statement.addBatch();

                if (i % 1000 == 0)
                    statement.executeBatch();
            }
            statement.executeBatch();
            connection.commit();

            speed = NOTES / ((System.currentTimeMillis() - start) / 1000.0);
        } catch (SQLException e) {
            throw new RestException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }

        LOG.info("batch: " + speed);

        return new ResponseEntity<>(Map.of("speed", speed), HttpStatus.CREATED);
    }
}
