package ru.nsu.fit.popov.springnsu1.rest;

import org.springframework.http.HttpStatus;

class RestException extends RuntimeException {

    private final HttpStatus status;

    RestException(HttpStatus status, String message) {
        super(message);
        this.status = status;
    }

    HttpStatus getStatus() {
        return status;
    }
}
