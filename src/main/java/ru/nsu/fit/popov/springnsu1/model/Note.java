package ru.nsu.fit.popov.springnsu1.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Date;

public class Note {

    public static final String TABLE = "notes";
    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_CONTENT = "content";

    public static final String DROP_TABLE = String.format("" +
            "DROP TABLE IF EXISTS %s",
            TABLE);
    public static final String CREATE_TABLE = String.format("" +
            "CREATE TABLE %s(" +
            "  id  INTEGER       PRIMARY KEY  AUTO_INCREMENT, " +
            "  %s  DATETIME      NOT NULL, " +                      //  date
            "  %s  VARCHAR(255)  NOT NULL" +                        //  content
            ")",
            TABLE, COLUMN_DATE, COLUMN_CONTENT);

    private final Date date;
    private final String content;

    @JsonCreator
    public Note(@JsonProperty(value = "date") Date date,
                @JsonProperty(value = "content") String content) {
        this.date = date;
        this.content = content;
    }

    public Date getDate() {
        return date;
    }

    public String getContent() {
        return content;
    }
}
